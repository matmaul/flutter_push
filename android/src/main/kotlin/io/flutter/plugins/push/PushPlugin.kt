// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package io.flutter.plugins.push

import android.app.Activity
import android.content.Context
import android.util.Log
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.PluginRegistry.Registrar
import com.google.firebase.iid.FirebaseInstanceId

class PushPlugin(context: Context, activity: Activity?) : MethodCallHandler {
    private val mContext = context

    companion object {
        @JvmStatic
        private val TAG = "PushPlugin"
        @JvmStatic
        val SHARED_PREFERENCES_KEY = "push_plugin_cache"
        @JvmStatic
        val MSG_CALLBACK_HANDLE_KEY = "msg_callback_handle"
        @JvmStatic
        val DELETED_MSGS_CALLBACK_HANDLE_KEY = "deleted_msgs_callback_handle"
        @JvmStatic
        val TOKEN_CALLBACK_HANDLE_KEY = "token_callback_handle"
        @JvmStatic
        val CALLBACK_DISPATCHER_HANDLE_KEY = "callback_dispatch_handler"

        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val plugin = PushPlugin(registrar.context(), registrar.activity())
            val channel = MethodChannel(registrar.messenger(), "plugins.flutter.io/push_plugin")
            channel.setMethodCallHandler(plugin)
        }

        @JvmStatic
        private fun getToken(context: Context, args: ArrayList<*>?): String? {
            return FirebaseInstanceId.getInstance().token
        }

        private fun saveCallbackHandle(context: Context, args: ArrayList<*>?, prefKey: String) {
            val callbackHandle = args!![0] as Long
            context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE)
                    .edit()
                    .putLong(prefKey, callbackHandle)
                    .apply()
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        val args = call.arguments() as? ArrayList<*>
        when(call.method) {
            "PushPlugin.initializeService" -> {
                Log.d(TAG, "Initializing PushService")
                saveCallbackHandle(mContext, args, CALLBACK_DISPATCHER_HANDLE_KEY)
                result.success(true)
            }
            "PushPlugin.setMessageReceivedCallback" -> {
                saveCallbackHandle(mContext, args, MSG_CALLBACK_HANDLE_KEY)
                result.success(true)
            }
            "PushPlugin.setDeletedMessagesCallback" -> {
                saveCallbackHandle(mContext, args, DELETED_MSGS_CALLBACK_HANDLE_KEY)
                result.success(true)
            }
            "PushPlugin.setNewTokenCallback" -> {
                saveCallbackHandle(mContext, args, TOKEN_CALLBACK_HANDLE_KEY)
                result.success(true)
            }
            "PushPlugin.getToken" -> {
                val token = getToken(mContext, args)
                result.success(token)
            }
            else -> result.notImplemented()
        }
    }
}
