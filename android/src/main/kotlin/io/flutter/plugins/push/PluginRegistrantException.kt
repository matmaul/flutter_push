package io.flutter.plugins.push

internal class PluginRegistrantException : RuntimeException(
        "PluginRegistrantCallback is not set. Did you forget to call PushService.setPluginRegistrant? See the README for instructions.")