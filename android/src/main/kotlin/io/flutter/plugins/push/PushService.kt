// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package io.flutter.plugins.push

import android.content.Context
import android.util.Log
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.view.FlutterCallbackInformation
import io.flutter.view.FlutterMain
import io.flutter.view.FlutterNativeView
import io.flutter.view.FlutterRunArguments
import java.util.ArrayDeque
import java.util.concurrent.atomic.AtomicBoolean

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage



class PushService : MethodCallHandler, FirebaseMessagingService() {
    class Call {
        var method = ""
        var args: List<Any?>? = null
    }

    private val queue = ArrayDeque<Call>()
    private lateinit var mBackgroundChannel: MethodChannel

    companion object {
        @JvmStatic
        private val TAG = "PushService"
        @JvmStatic
        private var sBackgroundFlutterView: FlutterNativeView? = null
        @JvmStatic
        private val sIsIsolateRunning = AtomicBoolean(false)

        @JvmStatic
        private lateinit var sPluginRegistrantCallback: PluginRegistrantCallback

        @JvmStatic
        fun setPluginRegistrant(callback: PluginRegistrantCallback) {
            sPluginRegistrantCallback = callback
        }
    }

    private fun startBackgroundIsolate(context: Context, callbackHandle: Long) {
        Log.d(TAG, "startBackgroundIsolate")
        FlutterMain.ensureInitializationComplete(context, null)
        val mAppBundlePath = FlutterMain.findAppBundlePath(context)
        val flutterCallback = FlutterCallbackInformation.lookupCallbackInformation(callbackHandle)
        if (flutterCallback == null) {
            Log.e(TAG, "Fatal: failed to find callback")
            return
        }

        sBackgroundFlutterView = FlutterNativeView(context, true)
        if (mAppBundlePath != null && !sIsIsolateRunning.get()) {
            if (sPluginRegistrantCallback == null) {
                throw PluginRegistrantException()
            }
            Log.i(TAG, "Starting PushService...")
            val args = FlutterRunArguments()
            args.bundlePath = mAppBundlePath
            args.entrypoint = flutterCallback.callbackName
            args.libraryPath = flutterCallback.callbackLibraryPath
            sBackgroundFlutterView!!.runFromBundle(args)
            sPluginRegistrantCallback.registerWith(sBackgroundFlutterView!!.pluginRegistry)
        }
    }

   override fun onMethodCall(call: MethodCall, result: Result) {
       when(call.method) {
            "PushService.initialized" -> {
                synchronized(sIsIsolateRunning) {
                    while (!queue.isEmpty()) {
                        val myCall = queue.remove()
                        mBackgroundChannel.invokeMethod(myCall.method, myCall.args)
                    }
                    sIsIsolateRunning.set(true)
                }
            }
            else -> result.notImplemented()
        }
        result.success(null)
    }

    override fun onCreate() {
        super.onCreate()

        val context = applicationContext
        FlutterMain.ensureInitializationComplete(context, null)

        if (!sIsIsolateRunning.get()) {
            val p = context.getSharedPreferences(PushPlugin.SHARED_PREFERENCES_KEY, 0)
            val callbackHandle = p.getLong(PushPlugin.CALLBACK_DISPATCHER_HANDLE_KEY, 0)
            startBackgroundIsolate(context, callbackHandle)
        }

        mBackgroundChannel = MethodChannel(sBackgroundFlutterView,
                "plugins.flutter.io/push_plugin_background")
        mBackgroundChannel.setMethodCallHandler(this)
    }

    private fun invokeOrQueue(method: String, args: List<Any?>) {
        synchronized(sIsIsolateRunning) {
            if (!sIsIsolateRunning.get()) {
                // Queue up callbacks while background isolate is starting
                val myCall = Call()
                myCall.method = method; myCall.args = args
                queue.add(myCall)
            } else {
                mBackgroundChannel.invokeMethod(method, args)
            }
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        invokeOrQueue("onMessageReceived", getMessageCallbackArgs(remoteMessage))
    }

    override fun onDeletedMessages() {
        val callbackHandle = applicationContext.getSharedPreferences(
                PushPlugin.SHARED_PREFERENCES_KEY,
                Context.MODE_PRIVATE)
                .getLong(PushPlugin.DELETED_MSGS_CALLBACK_HANDLE_KEY, 0)

        invokeOrQueue("onDeletedMessages", listOf(callbackHandle))
    }

    override fun onNewToken(token: String) {
        invokeOrQueue("onNewToken", getNewTokenCallbackArgs(token))
    }

    private fun getMessageCallbackArgs(remoteMessage: RemoteMessage): List<Any?> {
        val callbackHandle = applicationContext.getSharedPreferences(
                PushPlugin.SHARED_PREFERENCES_KEY,
                Context.MODE_PRIVATE)
                .getLong(PushPlugin.MSG_CALLBACK_HANDLE_KEY, 0)

        return listOf(callbackHandle, remoteMessage.data,
                remoteMessage.from, remoteMessage.from,
                remoteMessage.messageId, remoteMessage.messageType, remoteMessage.collapseKey,
                remoteMessage.priority, remoteMessage.originalPriority, remoteMessage.sentTime)
    }

    private fun getNewTokenCallbackArgs(token: String): List<Any?> {
        val callbackHandle = applicationContext.getSharedPreferences(
                PushPlugin.SHARED_PREFERENCES_KEY,
                Context.MODE_PRIVATE)
                .getLong(PushPlugin.TOKEN_CALLBACK_HANDLE_KEY, 0)

        return listOf(callbackHandle, token)
    }
}
