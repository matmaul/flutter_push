import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'package:push/src/push.dart';

// When we start the background service isolate, we only ever enter it once.
// To communicate between the native plugin and this entrypoint, we'll use
// MethodChannels to open a persistent communication channel to trigger
// callbacks.
void callbackDispatcher() {
  const MethodChannel _channel = MethodChannel('plugins.flutter.io/push_plugin_background');

  // Setup Flutter state needed for MethodChannels.
  WidgetsFlutterBinding.ensureInitialized();

  // This is where the magic happens and we handle background events from the
  // native portion of the plugin. Here we massage the message data into a
  // `Message` object which we then pass to the provided callback.
  _channel.setMethodCallHandler((MethodCall call) async {
    final dynamic args = call.arguments;

    Function _performCallbackLookup() {
      final CallbackHandle handle =
      CallbackHandle.fromRawHandle(call.arguments[0]);

      // PluginUtilities.getCallbackFromHandle performs a lookup based on the
      // handle we retrieved earlier.
      final Function closure = PluginUtilities.getCallbackFromHandle(handle);

      if (closure == null) {
        print('Fatal Error: Callback lookup failed!');
        exit(-1);
      }
      return closure;
    }

    Function method;
    if (call.method == 'onMessageReceived') {
      method ??= _performCallbackLookup();
      final PushMessage msg =
      PushMessage(Map<String, dynamic>.from(args[1]), args[2], args[3],
          args[4], args[5], args[6], args[7], args[8], args[9]);
      method(msg);
    } else if (call.method == 'onNewToken') {
      method ??= _performCallbackLookup();
      method(args[1]);
    } else if (call.method == 'onDeletedMessages') {
      method ??= _performCallbackLookup();
      method();
    } else {
      assert(false, "No handler defined for method type: '${call.method}'");
    }
  });

  _channel.invokeMethod('PushService.initialized');
}