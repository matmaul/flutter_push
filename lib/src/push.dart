import 'dart:async';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:push/src/callback_dispatcher.dart';

class PushMessage {
  final Map<String, dynamic> data;
  final String from, to, messageId, messageType, collapseKey;
  final int priority, originalPriority, sentTime;

  PushMessage(this.data, this.from, this.to, this.messageId, this.messageType,
      this.collapseKey, this.priority, this.originalPriority, this.sentTime);
}

class PushManager {
  static const MethodChannel _channel = MethodChannel('plugins.flutter.io/push_plugin');

  /// Initialize the plugin and request relevant permissions from the user.
  static Future<void> initialize() async {
    final CallbackHandle callbackHandle =
        PluginUtilities.getCallbackHandle(callbackDispatcher);
    return _channel.invokeMethod('PushPlugin.initializeService',
        <dynamic>[callbackHandle.toRawHandle()]);
  }

  static Future<void> setMessageReceivedCallback(Function(PushMessage msg) msgRcvCallback) async {
    final CallbackHandle callbackHandle =
    PluginUtilities.getCallbackHandle(msgRcvCallback);
    return _channel.invokeMethod('PushPlugin.setMessageReceivedCallback',
        <dynamic>[callbackHandle.toRawHandle()]);
  }

  static Future<void> setDeletedMessagesCallback(Function deletedMsgsCallback) async {
    final CallbackHandle callbackHandle =
    PluginUtilities.getCallbackHandle(deletedMsgsCallback);
    return _channel.invokeMethod('PushPlugin.setDeletedMessagesCallback',
        <dynamic>[callbackHandle.toRawHandle()]);
  }

  static Future<void> setNewTokenCallback(Function(String token) tokenCallback) async {
    final CallbackHandle callbackHandle =
    PluginUtilities.getCallbackHandle(tokenCallback);
    return _channel.invokeMethod('PushPlugin.setNewTokenCallback',
        <dynamic>[callbackHandle.toRawHandle()]);
  }

  static Future<String> getToken() async {
    return _channel.invokeMethod('PushPlugin.getToken');
  }
}
